// we use pg library to
// request connection pool from postgres database
// psql -h traineedb.cgq0reqixqsd.us-east-1.rds.amazonaws.com -d postgres -U traineeUser password is traineePassword
const { Pool } = require('pg')

// we connect to pg using pool we requested
const pool = new Pool({
  user: 'traineeUser',
  host: 'traineedb.cgq0reqixqsd.us-east-1.rds.amazonaws.com',
  password: 'traineePassword',
  database: 'teamalexander',
  port: 5432,
  multipleStatements: true
})

// the pool emits an error on behalf of any idle clients
pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})

// if no error on idel client, pool connects to database
pool.connect((err, client, done) => {
    //if there is an error with our database connection strings
    if (err) {
        console.error('Database connection failed: ' + err.stack);
        return;
    }
    //if no error then we have successfully connected 
    console.log('Connected to database');
    // do not call done(); - because we want to use this connection 
    // to create/insert/delete or select records from our database
    // sometime we might also want to export this connection for other resources
});

// insert a record into our table
pool.query(
    `INSERT INTO "Users"
                 (id, first_name, last_name, username, role, email, "createdAt", "updatedAt")
                 VALUES 
                 ('1', 'Michael', 'Uwa', 'muwa', 'PM' ,'mikeuwa@manifest.com', '2021-01-01','2021-01-01'),
                 ('2', 'Solomon', 'Baba', 'solobaba', 'QA' , 'solobaba@manifest.com', '2021-01-03','2021-01-03'),
                 ('3', 'Chuka', 'Eze', 'chueze', 'PM' , 'chueze@manifest.com', '2021-01-04', '2021-01-04')
                 `,
    (err, res) => {
      if(err) {
        console.log('Error or issue with table creation');
        console.log(err);
    } else {
        console.log('Inserted data into table successfully')
        console.log(res);
   }
  } 
);

                 // ('4', 'Tolu', 'Juwon', 'toluju', 'Dev' , 'toluju@manifest.com', '2021-01-07'),
                 // ('5', 'Idibia', 'Maraba', 'idmaraba', 'QA' , 'idmaraba@manifest.com', '2021-01-05'),
                 // ('6', 'Tony', 'Rufai', 'tonyrufai', 'QA' , 'tonyrufai@manifest.com', '2021-01-01'),
                 // ('7', 'Sam', 'Okoba', 'samokoba', 'QA' , 'samokoba@manifest.com', '2021-01-07'),
                 // ('8', 'Chioma', 'Ajunwa', 'chiaju', 'Dev' , 'chiaju@manifest.com', '2021-01-') 

pool.end();


// export connection
module.exports = pool;