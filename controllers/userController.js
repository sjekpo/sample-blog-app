var User = require('../models/user');
var models = require('../models');

// Display user create form on GET.
exports.user_create_get = async function (req, res, next) {
   // create user GET controller logic here
   // Finds the validation errors in this request and wraps them in an object with handy functions
    res.render('forms/user_form', { title: 'GET function to create user',  layout: 'layouts/detail'});
};
        

// Handle user create on POST.
exports.user_create_post = async function (req, res, next) {
   // create user GET controller logic here
   // Finds the validation errors in this request and wraps them in an object with handy functions
    try {
        const {first_name,last_name,username,role,email} = req.body;

        const user = await models.User.create({ first_name,last_name,username,role,email });
        if (user){
            res.redirect("/blog/user");
        }    
        
    } catch (error) {
        throw error;
        console.log(error);
    }
};

// Display user delete form on GET.
exports.user_delete_get = async function(req, res, next) {
    // POST logic to delete an user here
    // If an user gets deleted successfully, we just redirect to users list
    // no need to render a page

    try {
        
        const userDelete = await models.User.findByPk({ 
            where:
            {
                id:req.params.id,
            } 
        });

        res.render('pages/user_delete', { title: 'GET function to delete user', userDelete, layout: 'layouts/detail'});
        
    } catch (error) {
        throw error;
        console.log(error);
    }
        
};


// Handle user delete on POST.
exports.user_delete_post = async function(req, res, next) {
    // POST logic to delete an user here
    // If an user gets deleted successfully, we just redirect to users list
    // no need to render a page

    try {
        
        const userDelete = await models.User.destroy({ 
            where:
            {
                id:req.params.id,
            } 
        });

        res.redirect('/blog/user');
       
        
    } catch (error) {
        throw error;
        console.log(error);
    }
        
};

// Display user update form on GET.
exports.user_update_get = async function(req, res, next) {
        // GET logic to update an user here

        const id = req.params.id;

        const userUpdate = await models.User.findByPk({
            where:{id}
        });
        
        // renders user form
        res.render('forms/user_form', { title: 'GET function to update user', userUpdate, layout: 'layouts/detail'});
};

// Handle post update on POST.
exports.user_update_post = async function(req, res, next) {
        // POST logic to update an user here
        // If an user gets updated successfully, we just redirect to users list
        // no need to render a page

        const {first_name,last_name,username,role,email} = req.body,
        id = req.params.id;

        const userUpdate = await models.User.update({
            first_name,
            last_name,
            username,
            role,
            email
        },

        {
            where:{
                id
            }
        });

        if(userUpdate){
            res.redirect("/blog/user");
        }

};

// Display list of all users.
exports.user_list = async function(req, res, next) {
        // GET controller logic to list all users
       try{

        const users = await models.User.findAll();
        // renders all users list
        res.render('pages/user_list', { title: 'A list of all users', users,  layout: 'layouts/list'} );

       } catch (error){
         throw error;
         console.log(error);
       }
};

// Display detail page for a specific user.
exports.user_detail = async function(req, res, next) {
  // GET controller logic to display just one user
  
  // renders an individual user details page
    try {
        
        const  id = req.params.id;

        const userDetail = await models.User.findOne({ where:{id} });

        res.render('pages/user_detail', { title: 'A single user record', userDetail,  layout: 'layouts/detail'} );
    } catch (error) {
        throw error;
        console.log(error);
    }
        
};

exports.index = function(req, res) {
        // So, render user index page
        res.render('pages/user_index', { title: 'User Homepage', layout: 'layouts/main'} );
};

exports.user_all_route = function(req, res) {
        // So, redirect to user index page
         res.redirect("/blog/user");
};
 